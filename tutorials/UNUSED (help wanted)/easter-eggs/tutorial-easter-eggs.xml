<book>

<chapter id="EASTER-EGGS">
<title>Easter Eggs</title>


<abstract>
<para>
This tutorial covers the creation of Easter Eggs — basic elliptical shapes that are formed 
using bezier lines. 
</para>
</abstract>

<sect1>
<title>Creating the Egg</title>

<para>
To make the creation of the egg points easier, turn on the grid and snap. Then create 
the basic egg shape using the bezier pencil.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f01.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Use the node selection tool, and make the nodes smooth.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f02.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
The egg looks a little too fat.  Use the node tool and extend the bottom and top node a bit.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f03.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Color our egg, and fatten the outline using the Fill and Stroke tool.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f04.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>

<sect1>
<title>Adding stripes to the Egg</title>

<para>
Start with the Rectangle tool and draw a rectangle across the egg.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f05.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Select both objects using the <keycap>Shift</keycap> key while selecting.  Perform 
Path Division on the objects.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f06.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Eggs are curved surfaces, so adjust the curve of the band by first inserting a node in 
the middle of the band.  Using the node tool, select nodes on both sides of the band 
using <keycap>Shift</keycap>, and then use the Insert new nodes into selected 
segments command.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f07.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Using the node selection tool, make the band angles into smooth curves.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f08.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Color the band a different color by selecting the band, and using the Fill and Stroke options.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f09.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Add some designs to the band.  A star will look great on our egg. After copying the stars 
onto the egg, use the node tool to adjust the tilt of the star to match the curve of the egg.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f10.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
An Easter egg by itself is great, but having several eggs in a frame would be even better. 
Also, having an upright egg is nice, but tilting it a little will give it some character. 
Having several eggs that are identical is nice, but having a little variety is nicer.  Lets copy 
our egg, and adjust some things.  Group together all the objects in each egg when finished.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f11.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
After using the handles to adjust the size of the eggs so there is a little variety, use the 
handles to rotate each egg to its desired position.  Raise or lower each egg to get the 
desired perspective.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f12.svg"/>
</imageobject>
</mediaobject>
</figure>

<para>
Use the rectangle tool to frame our eggs and add a background.  Use the Fill and Stroke 
tool to adjust the border size and the background color. Lower the background rectangle 
behind the eggs, and adjust the edges to meet the eggs at the bottom and the sides.
</para>

<figure>
<mediaobject>
<imageobject><imagedata fileref="easter-eggs-f13.svg"/>
</imageobject>
</mediaobject>
</figure>

</sect1>
</chapter>
</book>
