# Translation of Inkscape's tips tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2020-09-04 19:55-0400\n"
"PO-Revision-Date: 2018-04-28 10:40+0300\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>, 2018\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#: tips-f06.svg:136(format) tips-f05.svg:558(format) tips-f04.svg:70(format)
#: tips-f03.svg:49(format) tips-f02.svg:49(format) tips-f01.svg:49(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tutorial-tips.xml:7(title)
msgid "Tips and Tricks"
msgstr "Поради та прийоми"

#: tutorial-tips.xml:8(subtitle)
msgid "Tutorial"
msgstr ""

#: tutorial-tips.xml:13(para)
msgid ""
"This tutorial will demonstrate various tips and tricks that users have "
"learned through the use of Inkscape and some “hidden” features that can help "
"you speed up production tasks."
msgstr ""
"В цьому уроці продемонструються різні поради та прийоми, які користувачі вже "
"вивчили через використання «Inkscape», та деякі «приховані» особливості, що "
"допоможуть виконувати робочі завдання швидше."

#: tutorial-tips.xml:21(title)
msgid "Radial placement with Tiled Clones"
msgstr "Радіальне розміщення за допомогою «Мозаїчних клонів»"

#: tutorial-tips.xml:22(para)
#, fuzzy
msgid ""
"It's easy to see how to use the <guimenuitem>Create Tiled Clones</"
"guimenuitem> dialog for rectangular grids and patterns. But what if you need "
"<firstterm>radial</firstterm> placement, where objects share a common center "
"of rotation? It's possible too!"
msgstr ""
"Легко зрозуміти як користуватися діалогом <command>Створити мозаїку з "
"клонів</command> для прямокутних сіток та шаблонів. Але що, якщо потрібне "
"<firstterm>радіальне</firstterm> розміщення, при якому об'єкти мають "
"спільний центр повороту? Це також можливо!"

#: tutorial-tips.xml:27(para)
msgid ""
"If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then "
"you can try the P3, P31M, P3M1, P4, P4M, P6, or P6M symmetries. These will "
"work nicely for snowflakes and the like. A more general method, however, is "
"as follows."
msgstr ""
"Якщо радіальний шаблон має містити 3, 4, 6 чи 12 елементів, то можна "
"спробувати симетрії типу P3, P31M, P3M1, P4, P4M, P6 чи P6M. Вони чудово "
"підійдуть для сніжинок і їм подібних. Більш загальний метод – далі."

#: tutorial-tips.xml:32(para)
#, fuzzy
msgid ""
"Choose the P1 symmetry (simple translation) and then <emphasis>compensate</"
"emphasis> for that translation by going to the <guimenuitem>Shift</"
"guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and "
"<guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will "
"be stacked exactly on top of the original. All that remains to do is to go "
"to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation angle "
"per column, then create the pattern with one row and multiple columns. For "
"example, here's a pattern made out of a horizontal line, with 30 columns, "
"each column rotated 6 degrees:"
msgstr ""
"Вибуріть симетрію типу P1 (просте переміщення) і потім "
"<emphasis>компенсуйте</emphasis> його перейшовши до вкладки «<command>Зсув</"
"command>» і виставивши <command>«На рядок»/«Зсув за віссю Y»</command> та "
"<command>«На стовпчик»/«Зсув за віссю X»</command> –100%. Тепер всі клони "
"будуть точно накладатися на оригінал. Все, що залишається зробити – це "
"перейти до вкладки «<command>Обертання</command>» і виставити певний кут на "
"стовпчик, потім створивши шаблон з одним рядком і декількома стовпчиками. "
"Наприклад, ось – шаблон, створений з горизонтальної лінії, з 30 стовпчиками, "
"кожен з яких повернутий на 6°:"

#: tutorial-tips.xml:49(para)
msgid ""
"To get a clock dial out of this, all you need to do is cut out or simply "
"overlay the central part by a white circle (to do boolean operations on "
"clones, unlink them first)."
msgstr ""
"Щоб створити з цього годинниковий циферблат, все, що потрібно – лише "
"вирізати або просто перекрити центральну частину білим кругом (щоб "
"застосовувати логічні операції до клонів, спочатку від'єднайте їх)."

#: tutorial-tips.xml:53(para)
msgid ""
"More interesting effects can be created by using both rows and columns. "
"Here's a pattern with 10 columns and 8 rows, with rotation of 2 degrees per "
"row and 18 degrees per column. Each group of lines here is a “column”, so "
"the groups are 18 degrees from each other; within each column, individual "
"lines are 2 degrees apart:"
msgstr ""
"Цікавіший ефект можна створити, використовуючи і рядки і стовпчики. Ось – "
"шаблон з 10 стовпчиків і 8 рядків з поворотом в 2° на рядок і 18° на "
"стовпчик. Тут кожна група ліній – «стовпчик», так, що групи розташовані "
"через 18° одна від одної; всередині кожного стовпчика, окремі лінії "
"розташовані з кроком в 2°:"

#: tutorial-tips.xml:67(para)
#, fuzzy
msgid ""
"In the above examples, the line was rotated around its center. But what if "
"you want the center to be outside of your shape? Just click on the object "
"twice with the Selector tool to enter rotation mode. Now move the object's "
"rotation center (represented by a small cross-shaped handle) to the point "
"you would like to be the center of the rotation for the Tiled Clones "
"operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the "
"object. This is how you can do nice “explosions” or “starbursts” by "
"randomizing scale, rotation, and possibly opacity:"
msgstr ""
"В приклдах вище, лінії оберталися відносно свого центру. Але що, якщо "
"потрібний центр – за межами фігури? Просто двічі клацніть по об'єкту за "
"допомогою інструменту «Селектор», щоб перейти в режим обертання. Тепер "
"перемістіть центр обертання об'єкту (представлений невеличким хрестоподібним "
"маніпулятором) до точки, яка має бути центром обертання для операції "
"«Мозаїчних клонів». Потім застосуйте до об'єкту «<command>Створити мозаїку з "
"клонів/command>». Ось як можна створити гарні «вибухи» чи «зоряні вибухи», "
"рандомізуючи масштаб, обертання і, можливо, прозорість:"

#: tutorial-tips.xml:85(title)
msgid "How to do slicing (multiple rectangular export areas)?"
msgstr "Як зробити розрізання (декілька прямокутних експортованих ділянок)?"

#: tutorial-tips.xml:86(para)
#, fuzzy
msgid ""
"Create a new layer, in that layer create invisible rectangles covering parts "
"of your image. Make sure your document uses the px unit (default), turn on "
"grid and snap the rects to the grid so that each one spans a whole number of "
"px units. Assign meaningful ids to the rects, and export each one to its own "
"file (<menuchoice><guimenu>File</guimenu><guimenuitem>Export PNG Image</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>E</keycap></"
"keycombo>)). Then the rects will remember their export filenames. After "
"that, it's very easy to re-export some of the rects: switch to the export "
"layer, use <keycap>Tab</keycap> to select the one you need (or use Find by "
"id), and click <guibutton>Export</guibutton> in the dialog. Or, you can "
"write a shell script or batch file to export all of your areas, with a "
"command like:"
msgstr ""
"Створіть новий шар, на ньому створіть невидимі прямокутники, що покривають "
"частини зображення. Переконайтесь, що Ваш документ використовує одиниці пк "
"(за замовчуванням), увімкніть сітку і прив'яжіть прямокутники до сітки так, "
"що кожен з них простягається на ціле число пк. Призначте прямокутникам "
"виразні ідентифікатори та експортуйте кожен з них до свого власного файлу "
"(<command>«Файл» &gt; «Експортувати як зображення PNG…»</command> "
"(<keycap>Shift+Ctrl+E</keycap>)). Потім прямокутники запам'ятають їхні "
"експортовані імена. Після чого дуже легко експортувати деякі прямокутники "
"знову: перейдіть до експортовауваного шару, використовуйте «Tab» для вибору "
"потрібного прямокутника (або використовуйте «Знайти за ідентифікатором») та "
"клацніть «Експортувати» в діалоговому вікні. Або можна написати сценарій "
"оболонки чи командний файл для експорту усіх ділянок за допомогою такої "
"команди як:"

#: tutorial-tips.xml:98(command)
msgid "inkscape -i area-id -t filename.svg"
msgstr "inkscape -i area-id -t filename.svg"

#: tutorial-tips.xml:100(para)
#, fuzzy
msgid ""
"for each exported area. The <command>-t</command> switch tells it to use the "
"remembered filename hint, otherwise you can provide the export filename with "
"the <command>-e</command> switch. Alternatively, you can use the "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Web</"
"guisubmenu><guimenuitem>Slicer</guimenuitem></menuchoice> extensions, or "
"<menuchoice><guimenu>Extensions</guimenu><guisubmenu>Export</"
"guisubmenu><guimenuitem>Guillotine</guimenuitem></menuchoice> for similar "
"results."
msgstr ""
"для кожної експортовуваної ділянки. Перемикач «‑t» наказує використовувати "
"збережену підказку імені файлу, інакше, для експортного імені можна "
"використовувати перемикач «‑e». Альтернативно, можна використовувати "
"<command>«Додатки» &gt; «Інтернет» &gt; «Розріз»</command> або "
"<command>«Додатки» &gt; «Експортувати» &gt; «Гільйотина»</command> для "
"схожих результатів."

#: tutorial-tips.xml:108(title)
msgid "Non-linear gradients"
msgstr "Нелінійні градієнти"

#: tutorial-tips.xml:109(para)
msgid ""
"The version 1.1 of SVG does not support non-linear gradients (i.e. those "
"which have a non-linear translations between colors). You can, however, "
"emulate them by <firstterm>multistop</firstterm> gradients."
msgstr ""
"Версія 1.1 формату SVG не підтримує нілінійні градієнти (тобто ті, що "
"містять нелінійні переходи між кольорами). Однак, їх можна імітувати "
"градієнтами з <firstterm>декількма опорними точками</firstterm>."

#: tutorial-tips.xml:114(para)
#, fuzzy
msgid ""
"Start with a simple two-stop gradient (you can assign that in the Fill and "
"Stroke dialog or use the gradient tool). Now, with the gradient tool, add a "
"new gradient stop in the middle; either by double-clicking on the gradient "
"line, or by selecting the square-shaped gradient stop and clicking on the "
"button <guimenuitem>Insert new stop</guimenuitem> in the gradient tool's "
"tool bar at the top. Drag the new stop a bit. Then add more stops before and "
"after the middle stop and drag them too, so that the gradient looks smooth. "
"The more stops you add, the smoother you can make the resulting gradient. "
"Here's the initial black-white gradient with two stops:"
msgstr ""
"Почніть з простого градієнту з двома опорними точками (можна призначити їх в "
"діалозі «Заповнення та штрих» або використовуючи інструмент «Градієнт»). "
"Тепер, за допомогою інструменту «Градієнт», додайте нову опорну точку "
"посередині; або двічі клацнувши по лінії градієнту або вибравши опорну точку "
"градієнту у формі квадрата і клачнувши кнопку «<command>Вставити нову опорну "
"точку</command>» на панелі інструментів інструменту «Градієнт» вгорі. Трохи "
"перетягніть нову опорну точку. Потім додайте ще опорних точок до та після "
"середньої точки і також поперетягуйте їх так, щоб градієнт здавався плавним. "
"Що більше опорних точок буде додано, то плавнішим буде результуючий "
"градієнт. Ось – початковий чорно-білий градієнт з двома опорними точками:"

#: tutorial-tips.xml:131(para)
msgid ""
"And here are various “non-linear” multi-stop gradients (examine them in the "
"Gradient Editor):"
msgstr ""
"А ось – різні «нелінійні» градієнти з багатьма опорними точками (дослідіть "
"їх в «Редакторі градієнтів»):"

#: tutorial-tips.xml:145(title)
msgid "Excentric radial gradients"
msgstr "Ексцентричні радіальні градієнти"

#: tutorial-tips.xml:146(para)
#, fuzzy
msgid ""
"Radial gradients don't have to be symmetric. In Gradient tool, drag the "
"central handle of an elliptic gradient with <keycap function=\"shift"
"\">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</"
"firstterm> of the gradient away from its center. When you don't need it, you "
"can snap the focus back by dragging it close to the center."
msgstr ""
"Радіальні градієнти не мають бути симетричними. В інструменті «Градієнт», "
"перетягніть центральний маніпулятор еліптичного градієнту натиснувши "
"<keycap>Shift</keycap>. Це перемістить x‑подібний <firstterm>фокус "
"маніпулятора</firstterm> градієнту від центру. Коли це непотрібно, можна "
"повернути фокус на початкове місце, натиснувши його близько до центру."

#: tutorial-tips.xml:162(title)
msgid "Aligning to the center of the page"
msgstr "Виріснювання по центру сторінки"

#: tutorial-tips.xml:163(para)
#, fuzzy
msgid ""
"To align something to the center or side of a page, select the object or "
"group and then choose <guimenuitem>Page</guimenuitem> from the "
"<guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and "
"Distribute</guimenuitem> dialog (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>A</keycap></"
"keycombo>)."
msgstr ""
"Щоб вирівняти що-небудь по центру чи одній зі сторі сторінки, виділіть "
"об'єкт або групу і виберіть «<command>Сторінку</command>» зі списку "
"«<command>Відносно:</command>» діалогу «Вирівняти та "
"розподілити…» (<keycap>Shift+Ctrl+A</keycap>)."

#: tutorial-tips.xml:171(title)
msgid "Cleaning up the document"
msgstr "Очищення документу"

#: tutorial-tips.xml:172(para)
#, fuzzy
msgid ""
"Many of the no-longer-used gradients, patterns, and markers (more precisely, "
"those which you edited manually) remain in the corresponding palettes and "
"can be reused for new objects. However if you want to optimize your "
"document, use the <guimenuitem>Clean Up Document</guimenuitem> command in "
"<guimenu>File</guimenu> menu. It will remove any gradients, patterns, or "
"markers which are not used by anything in the document, making the file "
"smaller."
msgstr ""
"Багато градієнтів, шаблонів та маркерів, що більше не використовуються "
"(точніше ті, які було відредаговано вручну) залишаються у відповідних "
"палітрах і можуть бути використані знову для нових об'єктів. Однак, якщо Ви "
"хочете оптимізувати Ваш документ, скористайтеся командою «<command>Очистити "
"документ</command>» в меню «Файл». Вона видалить всі градієнти, шаблони чи "
"маркери, які не використовуються в документі і, таким чином, зробить файл "
"меншим."

#: tutorial-tips.xml:181(title)
msgid "Hidden features and the XML editor"
msgstr "Приховані функції «Редактора XML»"

#: tutorial-tips.xml:182(para)
#, fuzzy
msgid ""
"The XML editor (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>) allows you "
"to change almost all aspects of the document without using an external text "
"editor. Also, Inkscape usually supports more SVG features than are "
"accessible from the GUI. The XML editor is one way to get access to these "
"features (if you know SVG)."
msgstr ""
"«Редактор XML» (<keycap>Shift+Ctrl+X</keycap>) дозволяє змінювати практично "
"всі параметри без використання зовнішнього текстового редактора. Також, "
"«Inkscape», зазвичай, підтримує більше властивостей SVG, ніж доступно з "
"графічного інтерфейсу. «Редактор XML» – єдиний спосіб отримати доступ до цих "
"властивостей (якщо Ви знайомі з SVG)."

#: tutorial-tips.xml:191(title)
msgid "Changing the rulers' unit of measure"
msgstr "Зміна одиниць вимірювання лінійки"

#: tutorial-tips.xml:192(para)
#, fuzzy
msgid ""
"In the default template, the unit of measure used by the rulers is mm. This "
"is also the unit used in displaying coordinates at the lower-right corner "
"and preselected in all units menus. (You can always hover your "
"<mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the "
"units it uses.) To change this, open <guimenuitem>Document Properties</"
"guimenuitem> (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>D</keycap></keycombo>) and change "
"the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Page</"
"guimenuitem> tab."
msgstr ""
"В шаблоні за замовчуванням, одиниці вимірювання, що використовуються для "
"лінійок – мм. Також в цих одиницях показуються координати в лівому нижньому "
"кутку і вони попередньо вибрані в усіх меню одиниць вимірювання. (Завжди "
"можна навести вказівник миші на лінійку і після затримки відобразиться "
"спливаюча підказка з одиницями вимірювання.) Щоб зміити їх, відкрийте "
"«<command>Параметри документа…</command>» (<keycap>Shift+Ctrl+D</keycap>) та "
"змініть «<command>Одиниці вимірювання</command>» на вкладці "
"«<command>Сторінка</command>»."

#: tutorial-tips.xml:203(title)
msgid "Stamping"
msgstr "Штампування"

#: tutorial-tips.xml:204(para)
#, fuzzy
msgid ""
"To quickly create many copies of an object, use <firstterm>stamping</"
"firstterm>. Just drag an object (or scale or rotate it), and while holding "
"the <mousebutton role=\"click\">mouse</mousebutton> button down, press "
"<keycap>Space</keycap>. This leaves a “stamp” of the current object shape. "
"You can repeat it as many times as you wish."
msgstr ""
"Щоб швидко створити багато копій об'єкту, використовуйте "
"<firstterm>штампування</firstterm>. Просто перетягуйте об'єкт (або "
"масштабуйте чи повертайте його) і, утримуючи кнопку миші, натисніть "
"«<keycap>Пробіл</keycap>». Це залишить «штамп» поточної форми об'єкту. Можна "
"повторювати цю дію стільки скільки потрбіно."

#: tutorial-tips.xml:213(title)
msgid "Pen tool tricks"
msgstr "Прийоми інструменту «Перо»"

#: tutorial-tips.xml:214(para)
msgid ""
"In the Pen (Bezier) tool, you have the following options to finish the "
"current line:"
msgstr ""
"Для інструменту «Перо» (Безьє), є наступні опції для завершення поточної "
"лінії:"

#: tutorial-tips.xml:218(para)
msgid "Press <keycap>Enter</keycap>"
msgstr "натиснути «<keycap>Enter</keycap>»;"

#: tutorial-tips.xml:221(para)
#, fuzzy
msgid ""
"<mousebutton role=\"double-click\">Double click</mousebutton> with the left "
"mouse button"
msgstr "подвійне калацання лівою кнопкою миші;"

#: tutorial-tips.xml:224(para)
msgid ""
"Click with the <mousebutton role=\"right-click\">right</mousebutton> mouse "
"button"
msgstr ""

#: tutorial-tips.xml:227(para)
msgid "Select another tool"
msgstr "вибрати інший інструмент."

#: tutorial-tips.xml:232(para)
#, fuzzy
msgid ""
"Note that while the path is unfinished (i.e. is shown green, with the "
"current segment red) it does not yet exist as an object in the document. "
"Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole "
"path) or <keycap>Backspace</keycap> (remove the last segment of the "
"unfinished path) instead of <guimenuitem>Undo</guimenuitem>."
msgstr ""
"Зверніть увагу, що поки контур незавершений (тобто, відображається зеленим з "
"поточним червоним сегментом), він ще не існує як об'єкт в документі. Тому, "
"щоб скасувати його, використовуйте «<keycap>Esc</keycap>» (cкасовує весь "
"контур) або «<keycap>Backspace</keycap>» (видаляє останній сегмент "
"незавершеного контуру) замість «<command>Вернути</command>»."

#: tutorial-tips.xml:239(para)
#, fuzzy
msgid ""
"To add a new subpath to an existing path, select that path and start drawing "
"with <keycap function=\"shift\">Shift</keycap> from an arbitrary point. If, "
"however, what you want is to simply <emphasis>continue</emphasis> an "
"existing path, Shift is not necessary; just start drawing from one of the "
"end anchors of the selected path."
msgstr ""
"Щоб додати новий підконтур до існуючого контуру, виділіть той контур і "
"почніть малювати з «<keycap>Shift</keycap>» від довільної точки. Якщо, "
"однак, все, що потрібно – це просто <emphasis>продовжити</emphasis> існуючий "
"контур, «Shift» – непотрібний; просто почніть малювати від одного з кінцевих "
"якорів виділеного контуру."

#: tutorial-tips.xml:248(title)
msgid "Entering Unicode values"
msgstr "Введення значень «Unicode»"

#: tutorial-tips.xml:249(para)
#, fuzzy
msgid ""
"While in the Text tool, pressing <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>U</keycap></keycombo> toggles between Unicode and "
"normal mode. In Unicode mode, each group of 4 hexadecimal digits you type "
"becomes a single Unicode character, thus allowing you to enter arbitrary "
"symbols (as long as you know their Unicode codepoints and the font supports "
"them). To finish the Unicode input, press <keycap>Enter</keycap>. For "
"example, <keycombo action=\"seq\"><keycap function=\"control\">Ctrl</"
"keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</"
"keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo> inserts an em-"
"dash (—). To quit the Unicode mode without inserting anything press "
"<keycap>Esc</keycap>."
msgstr ""
"При активному інструменті «Текст», натискання <keycap>Ctrl+U</keycap> "
"перемикає між режимами «Unicode» звичайним. В режимі «Unicode», кожна група "
"з 4‑х шістнадцяткових цифр, що вводяться, стають єдиним символом «Unicode», "
"дозволяючи таким чином вводити довільні символи (допоки Ви знаєте їхній  "
"Unicode-код і шрифт підтримує їх). Для завершення введення «Unicode», "
"натисніть «<keycap>Enter</keycap>». Наприклад, <keycap>Ctrl+U 2 0 1 4 Enter</"
"keycap> вставляє довге тире (—). Для виходу з режиму «Unicode» без введення "
"будь-чого, натисніть «<keycap>Esc</keycap>»."

#: tutorial-tips.xml:258(para)
#, fuzzy
msgid ""
"You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode "
"Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs "
"into your document."
msgstr ""
"Також мжна користуватися діалогом <command>«Текст» &gt; «Гліфи…»</command> "
"для пошуку і вставки гліфів в документ."

#: tutorial-tips.xml:265(title)
msgid "Using the grid for drawing icons"
msgstr "Використання сітки для малювання піктограм"

#: tutorial-tips.xml:266(para)
#, fuzzy
msgid ""
"Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use "
"the <guimenuitem>Document Preferences</guimenuitem>) and set the grid to 0.5 "
"px (48x48 gridlines). Now, if you align filled objects to <emphasis>even</"
"emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> "
"gridlines with the stroke width in px being an even number, and export it at "
"the default 96dpi (so that 1 px becomes 1 bitmap pixel), you get a crisp "
"bitmap image without unneeded antialiasing."
msgstr ""
"Припустимо, потрібно створити піктограму розміром 24×24 пікселі. Створіть "
"полотно розміром 24×24 пікселі (використовуйте «<command>Параметри документа…"
"</command>») і виставіть розмір сітки на 0,5 пікселі (48×48 ліній сітки). "
"Тепер, якщо вирівняти заповнені об'єкти по <emphasis>парним</emphasis> ліням "
"сітки, а штрихові об'єкти – по <emphasis>непарним</emphasis>, з шириною "
"штриха вказаною парним числом пікселів, і експортувати це з роздільною "
"здатністю за замовчуванням 96 точок на дюйм (так, що 1 піксель стає "
"1 растровим пікселем), то отримаємо чітко окреслене растрове зображення без "
"непотрібного згладжування."

#: tutorial-tips.xml:277(title)
msgid "Object rotation"
msgstr "Обертання об'єкту"

#: tutorial-tips.xml:278(para)
#, fuzzy
msgid ""
"When in the Selector tool, <mousebutton role=\"click\">click</mousebutton> "
"on an object to see the scaling arrows, then <mousebutton role=\"click"
"\">click</mousebutton> again on the object to see the rotation and skew "
"arrows. If the arrows at the corners are clicked and dragged, the object "
"will rotate around the center (shown as a cross mark). If you hold down the "
"<keycap function=\"shift\">Shift</keycap> key while doing this, the rotation "
"will occur around the opposite corner. You can also drag the rotation center "
"to any place."
msgstr ""
"При активному інструменті «Селектор», <keycap>клацніть</keycap> по об'єкту, "
"щоб відобразилися стрілки масштабування, потім <keycap>клацніть знову</"
"keycap> по об'єкту, щоб відобразилися стрілки обертання і нахилу. Якщо "
"клацнути і потягнути за кутові стрілки, об'єкт буде обертатися відносно "
"центру (позначено маркером-хрестиком). Якщо під час йього утримувати клавішу "
"«<keycap>Shift</keycap>», то обертання буде здійснюватися відносно "
"протилежного кута. Можна перетягнути центр обертання в будь-яке місце."

#: tutorial-tips.xml:287(para)
#, fuzzy
msgid ""
"Or, you can rotate from keyboard by pressing <keycap>[</keycap> and "
"<keycap>]</keycap> (by 15 degrees) or <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>[</keycap></keycombo> and <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The "
"same <keycap>[</keycap><keycap>]</keycap> keys with <keycap function=\"alt"
"\">Alt</keycap> perform slow pixel-size rotation."
msgstr ""
"Або можна обертати з клавіатури натискаючи «<keycap>[</keycap>» та "
"«<keycap>]</keycap>» (на 15°) або <keycap>Ctrl+[</keycap> та <keycap>Ctrl+]</"
"keycap> (на 90°). Ці ж клавіші <keycap>[]</keycap> з «<keycap>Alt</keycap>», "
"виконують повільне обертання на піксельну величину."

#: tutorial-tips.xml:296(title)
msgid "Drop shadows"
msgstr "Відкидання тіней"

#: tutorial-tips.xml:297(para)
#, fuzzy
msgid ""
"To quickly create drop shadows for objects, use the "
"<menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows and Glows</"
"guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature."
msgstr ""
"Щоб швидко створити відкидання тіней для об'єктів, використовуйте функцію "
"<command>«Фільтри» &gt; «Тіні і відблиски» &gt; «Відкидання тіні…»</command>."

#: tutorial-tips.xml:301(para)
#, fuzzy
msgid ""
"You can also easily create blurred drop shadows for objects manually with "
"blur in the Fill and Stroke dialog. Select an object, duplicate it by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>D</keycap></"
"keycombo>, press <keycap>PgDown</keycap> to put it beneath original object, "
"place it a little to the right and lower than original object. Now open Fill "
"And Stroke dialog and change Blur value to, say, 5.0. That's it!"
msgstr ""
"Також можна мегко створити розмиті відкинуті тіні для об'єктів вручну за "
"допомогою розмивання в діалозі «Заповнення та штрих». Виділіть об'єкт, "
"продублюйте його за допомогою <keycap>Ctrl+D</keycap>, натисніть "
"«<keycap>PgDown</keycap>», для розміщення його під початковим об'єктом, "
"розмістіть його трохи справа й нижче початкового об'єкту. Тепер відкрийте "
"діалог «Заповнення та штрих» і змініть значення «Розмиття», скажімо, на 5,0. "
"Ось і все!"

#: tutorial-tips.xml:311(title)
msgid "Placing text on a path"
msgstr "Розміщення тексту на контурі"

#: tutorial-tips.xml:312(para)
#, fuzzy
msgid ""
"To place text along a curve, select the text and the curve together and "
"choose <guimenuitem>Put on Path</guimenuitem> from the <guimenu>Text</"
"guimenu> menu. The text will start at the beginning of the path. In general "
"it is best to create an explicit path that you want the text to be fitted "
"to, rather than fitting it to some other drawing element — this will give "
"you more control without screwing over your drawing."
msgstr ""
"Для розміщення тексту вздовж кривої, виділіть разом і текст і криву та "
"виберіть «<command>Розмістити по контуру</command>» з меню «Текст». Текст "
"буде починатися на початку контуру. Взагалі, найкраще створити чіткий "
"контур, до якого потрібно припасувати текст, аніж припасовувати текст до "
"якого-небудь іншого елементу малюнку – це надає більше контролю без зайвих "
"проблем."

#: tutorial-tips.xml:322(title)
msgid "Selecting the original"
msgstr "Виділення оригіналу"

#: tutorial-tips.xml:323(para)
#, fuzzy
msgid ""
"When you have a text on path, a linked offset, or a clone, their source "
"object/path may be difficult to select because it may be directly "
"underneath, or made invisible and/or locked. The magic key <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> will help "
"you; select the text, linked offset, or clone, and press <keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>D</keycap></keycombo> to move "
"selection to the corresponding path, offset source, or clone original."
msgstr ""
"Якщо є текст на контурі, пов'язаний відступ або клон, їхній початковий "
"об'єкт/контур може бути складно вибрати. тому що він може знаходитися "
"безпосередньо зниху або бути невидимим та/або замкнутим. Чарівні клавіші "
"<keycap>Shift+D</keycap> допоможуть в цьому; виділіть текст, пов'язаний "
"відступ або клон, і натисніть <keycap>Shift+D</keycap>, щоб виділити "
"відповідний контур, джерело відступу або оригінал клону."

#: tutorial-tips.xml:333(title)
msgid "Window off-screen recovery"
msgstr "Відновлення вікна, розташованого за межами екрану"

#: tutorial-tips.xml:334(para)
#, fuzzy
msgid ""
"When moving documents between systems with different resolutions or number "
"of displays, you may find Inkscape has saved a window position that places "
"the window out of reach on your screen. Simply maximise the window (which "
"will bring it back into view, use the task bar), save and reload. You can "
"avoid this altogether by unchecking the global option to save window "
"geometry (<guimenuitem>Inkscape Preferences</guimenuitem>, "
"<menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></"
"menuchoice> section)."
msgstr ""
"Переміщуючи документи між системами з різною роздільною здатністю чи "
"кількістю дисплеїв, то можна зіштовхнутися з тим, що «Inkscape» зберігає "
"положення вікна за межами дояжності Вашого екрану. Просто розгорніть вікно "
"(що поверне вікно в область видимості, використовуючи панель завдань), "
"збережіть і перезавантажте. Цього можна уникнути, прибравши позначку "
"глобальної опції збереження геометрії вікна («<command>Налаштування…</"
"command>», розділ <command>«Інтерфейс» &gt; «Вікна»</command>)."

#: tutorial-tips.xml:345(title)
msgid "Transparency, gradients, and PostScript export"
msgstr "Прозорість, градієнти та експорт «PostScript»"

#: tutorial-tips.xml:346(para)
#, fuzzy
msgid ""
"PostScript or EPS formats do not support <emphasis>transparency</emphasis>, "
"so you should never use it if you are going to export to PS/EPS. In the case "
"of flat transparency which overlays flat color, it's easy to fix it: Select "
"one of the transparent objects; switch to the Dropper tool (<keycap>F7</"
"keycap> or <keycap>d</keycap>); make sure that the <guilabel>Opacity: Pick</"
"guilabel> button in the dropper tool's tool bar is deactivated; click on "
"that same object. That will pick the visible color and assign it back to the "
"object, but this time without transparency. Repeat for all transparent "
"objects. If your transparent object overlays several flat color areas, you "
"will need to break it correspondingly into pieces and apply this procedure "
"to each piece. Note that the dropper tool does not change the opacity value "
"of the object, but only the alpha value of its fill or stroke color, so make "
"sure that every object's opacity value is set to 100% before you start out."
msgstr ""
"Формати «PostScript» чи «EPS» не підтримують <emphasis>прозорість</"
"emphasis>, тому не слід її використовувати, якщо збираєтеся експотувати файл "
"до «PS»/«EPS». У випадку простої прозорості, яка накладається на простий "
"колір, це просто виправити: виділіть один з прозорих об'єктів; активуйте "
"інструмент «Піпетка» («<keycap>F7</keycap>» або «<keycap>d</keycap>»); "
"переконайтеся, що кнопка «<command>Непрозорість: Піпетка</command>» на "
"панелі інструментів «Піпетка» деактивована; клацніть по тому ж об'єкту. Це "
"вибере видимий колір і знову призначить його об'єкту, але цього разу без "
"прозорості. Повторіть для всіх прозорих об'єктів. Якщо прозорий об'єкт "
"накладається на декілька інших ділянок з простим кольором, потрбіно буде "
"розбити його на шматки і застосувати цю процедуру до кожного шматка. "
"Зверніть увагу, що інструмент «Піпетка» не змінює значення непрозорості "
"об'єкту, а лише значення альфа-каналу кольору його заповнення або штриха, "
"тому перед початком переконайтеся, що значення непрозорості кожного об'єкту "
"виставлено на 100%."

#: tutorial-tips.xml:363(title)
msgid "Interactivity"
msgstr ""

#: tutorial-tips.xml:364(para)
msgid ""
"Most SVG elements can be tweaked to react to user input (usually this will "
"only work if the SVG is displayed in a web browser)."
msgstr ""

#: tutorial-tips.xml:365(para)
msgid ""
"The simplest possibility is to add a clickable link to objects. For this "
"<mousebutton role=\"right-click\">right-click</mousebutton> the object and "
"select <menuchoice><guimenuitem>Create Link</guimenuitem></menuchoice> from "
"the context menu. The \"Object attributes\" dialog will open, where you can "
"set the target of the link using the value of <guilabel>href</guilabel>."
msgstr ""

#: tutorial-tips.xml:366(para)
msgid ""
"More control is possible using the interactivity attributes accessible from "
"the \"Object Properties\" dialog (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap function=\"shift\">Shift</keycap><keycap>O</keycap></"
"keycombo>). Here you can implement arbitrary functionality using JavaScript. "
"Some basic examples:"
msgstr ""

#: tutorial-tips.xml:369(para)
msgid "Open another file in the current window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:371(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>"
msgstr ""

#: tutorial-tips.xml:375(para)
msgid "Open an arbitrary weblink in new window when clicking on the object:"
msgstr ""

#: tutorial-tips.xml:377(para)
msgid ""
"Set <guilabel>onclick</guilabel> to <code>window.open(\"https://inkscape.org"
"\",\"_blank\");</code>"
msgstr ""

#: tutorial-tips.xml:381(para)
msgid "Reduce transparency of the object while hovering:"
msgstr ""

#: tutorial-tips.xml:383(para)
msgid ""
"Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>"
msgstr ""

#: tutorial-tips.xml:384(para)
msgid "Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>"
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tips.xml:0(None)
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2018"

#~ msgid "Click with the right mouse button"
#~ msgstr "клацання правою кнопкою миші;"
